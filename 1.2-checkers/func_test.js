fetch('./checkers.wasm').then(response =>
    response.arrayBuffer()
).then(bytes => WebAssembly.instantiate(bytes, {
    events: {
        piecemoved: (x1, y1, x2, y2) => {
            console.log("Piece moved from (" + x1 + ", " + y1 + ") to ("
                        + x2 + ", " + y2 + ")");
        },
        piececrowned: (x, y) => {
            console.log("Piece at (" + x + ", " + y + ") crowned!");
        }
    },
})).then(results => {
    console.log("Loaded WASM module");
    instance = results.instance;

    const black = 1;
    const white = 2;
    const crowned_black = 5;
    const crowned_white = 7;

    console.log("Calling offset");
    var offset = instance.exports.offsetOf(3, 4);
    console.log("Offset of (3, 4) is: ", offset);

    console.debug("White is white?", instance.exports.isWhite(white));
    console.debug("Black is black?", instance.exports.isBlack(black));
    console.debug("Black is white?", instance.exports.isWhite(black));
    console.debug("Black is crowned?", instance.exports.isCrowned(black));
    console.debug("White is crowned?", instance.exports.isCrowned(white));
    console.debug("Crowned white?", instance.exports.isCrowned(crowned_white));
    console.debug("Crowned black?", instance.exports.isCrowned(crowned_black));

    console.debug("Crown white", instance.exports.isCrowned(
        instance.exports.addCrown(white)));
    console.debug("Crown black", instance.exports.isCrowned(
        instance.exports.addCrown(black)));

    console.debug("Remove crown white", instance.exports.isCrowned(
        instance.exports.rmCrown(crowned_white)));
    console.debug("Remove crown black", instance.exports.isCrowned(
        instance.exports.rmCrown(crowned_black)));

    console.debug("In range: (0, 0): ", instance.exports.areCoordsInRange(0, 0));
    console.debug("In range: (1, 6): ", instance.exports.areCoordsInRange(1, 6));
    console.debug("In range: (7, 7): ", instance.exports.areCoordsInRange(7, 7));
    console.debug("In range: (7, 8): ", instance.exports.areCoordsInRange(7, 8));
    console.debug("In range: (-1, 7): ", instance.exports.areCoordsInRange(-1, 7));

    console.debug("board[0, 0] == ", instance.exports.getPiece(0, 0));
    console.debug("board[0, 1] == ", instance.exports.getPiece(0, 1));
    instance.exports.setPiece(0, 1, 5);
    instance.exports.setPiece(4, 4, white);
    console.debug("board[0, 0] == ", instance.exports.getPiece(0, 0));
    console.debug("board[0, 1] == ", instance.exports.getPiece(0, 1));

    // Turn is initially 0
    console.debug("turn == ", instance.exports.getTurnOwner());
    console.debug("black's turn? == ", instance.exports.isPlayersPiece(black));
    console.debug("black's turn (c)? == ", instance.exports.isPlayersPiece(crowned_black));
    console.debug("white's turn? == ", instance.exports.isPlayersPiece(white));
    console.debug("white's turn (c)? == ", instance.exports.isPlayersPiece(crowned_white));

    instance.exports.setTurnOwner(white);
    console.debug("black's turn? == ", instance.exports.isPlayersPiece(black));
    console.debug("black's turn (c)? == ", instance.exports.isPlayersPiece(crowned_black));
    console.debug("white's turn? == ", instance.exports.isPlayersPiece(white));
    console.debug("white's turn (c)? == ", instance.exports.isPlayersPiece(crowned_white));

    instance.exports.switchTurnOwner();
    console.debug("black's turn? == ", instance.exports.isPlayersPiece(black));
    console.debug("black's turn (c)? == ", instance.exports.isPlayersPiece(crowned_black));
    console.debug("white's turn? == ", instance.exports.isPlayersPiece(white));
    console.debug("white's turn (c)? == ", instance.exports.isPlayersPiece(crowned_white));

    console.debug("shouldCrown(0, black) == ", instance.exports.shouldCrown(0, black));
    console.debug("shouldCrown(7, black) == ", instance.exports.shouldCrown(7, black));
    console.debug("shouldCrown(0, white) == ", instance.exports.shouldCrown(0, white));
    console.debug("shouldCrown(7, white) == ", instance.exports.shouldCrown(7, white));

    var moves = [
        [4, 4, 5, 5],
        [4, 4, 3, 3],
        [4, 4, 3, 5],
        [4, 4, 5, 3],
        [4, 4, 6, 6],
        [4, 4, 2, 2],
        [4, 4, 2, 6],
        [4, 4, 6, 2],
        [4, 4, 4, 4],
        [4, 4, 5, 4],
        [4, 4, 4, 6],
        [4, 4, 6, 3],
        [4, 4, 3, 2],
        [5, 6, 7, 4],
    ];

    for (move of moves) {
        const [x1, y1, x2, y2] = move;
        console.debug("(", x1, ",", y1, ") -> (", x2, ",", y2, ") == ",
            instance.exports.isValidJumpDistance(x1, y1, x2, y2));
    }
});
