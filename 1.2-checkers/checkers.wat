(module
  (import "events" "piececrowned"
        (func $notify_pieceCrowned (param $x i32) (param $y i32)))
  (import "events" "piecemoved"
        (func $notify_pieceMoved (param $piece i32)
                                 (param $sourceX i32) (param $sourceY i32)
                                 (param $targetX i32) (param $targetY i32)))

  (memory $mem 1)

  ;; Constants
  (global $BLACK i32 (i32.const 1))
  (global $WHITE i32 (i32.const 2))
  (global $CROWN i32 (i32.const 4))

  ;; Global variables
  (global $currentTurn (mut i32) (i32.const 0))


  ;; Game board indexing
  (func $indexOf (param $x i32) (param $y i32) (result i32)
        (i32.add (get_local $x) (i32.mul (get_local $y) (i32.const 8))))

  (func $offsetOf (param $x i32) (param $y i32) (result i32)
        (i32.mul (call $indexOf (get_local $x) (get_local $y))
                 (i32.const 4)))

  ;; Returns true if: $low <= $value <= $high
  (func $isInRange (param $low i32)
                   (param $high i32)
                   (param $value i32) (result i32)
        (i32.and
          (i32.ge_s (get_local $value) (get_local $low))
          (i32.le_s (get_local $value) (get_local $high))))

  ;; Returns true if $coord is a valid x or y board coordinate.
  (func $isCoordInRange (param $coord i32) (result i32)
        (call $isInRange (i32.const 0) (i32.const 7) (get_local $coord)))

  ;; Returns true if *both* $x and $y are valid x and y board coordinates.
  (func $areCoordsInRange (param $x i32) (param $y i32) (result i32)
        (i32.and
          (call $isCoordInRange (get_local $x))
          (call $isCoordInRange (get_local $y))))

  ;; Low level bit test, set and clear funcs.
  (func $isSet (param $input i32) (param $bits i32) (result i32)
        (i32.eq 
          (i32.and (get_local $input) (get_local $bits))
          (get_local $bits)))

  (func $setBits (param $input i32) (param $bits i32) (result i32)
        (i32.or (get_local $input) (get_local $bits)))

  (func $clrBits (param $input i32) (param $bits i32) (result i32)
        (i32.and (get_local $input)
                 (i32.xor (get_local $bits) (i32.const -1))))

  ;; Higher level game flags test, set and clear.
  (func $isBlack (param $piece i32) (result i32)
        (call $isSet (get_local $piece) (get_global $BLACK))) 

  (func $isWhite (param $piece i32) (result i32)
        (call $isSet (get_local $piece) (get_global $WHITE))) 

  (func $isCrowned (param $piece i32) (result i32)
        (call $isSet (get_local $piece) (get_global $CROWN))) 

  (func $addCrown (param $piece i32) (result i32)
        (call $setBits (get_local $piece) (get_global $CROWN)))

  (func $rmCrown (param $piece i32) (result i32)
        (call $clrBits (get_local $piece) (get_global $CROWN)))


  ;; Game state manipulation
  ;; -----------------------
  (func $getTurnOwner (result i32)
        (get_global $currentTurn))

  (func $setTurnOwner (param $player i32)
        (set_global $currentTurn (get_local $player)))

  (func $switchTurnOwner
        (if (i32.eq (call $getTurnOwner) (get_global $BLACK))
          (then (call $setTurnOwner (get_global $WHITE)))
          (else (call $setTurnOwner (get_global $BLACK)))))

  (func $isPlayersPiece (param $piece i32) (result i32)
        (i32.gt_s
          (i32.and (call $getTurnOwner) (get_local $piece))
          (i32.const 0)))

  ;; Set the state of a square on the board.
  (func $setPiece (param $x i32) (param $y i32) (param $piece i32)
        (i32.store (call $offsetOf (get_local $x) (get_local $y))
                   (get_local $piece)))

  ;; Get the currect state of a square on the board.
  (func $getPiece (param $x i32) (param $y i32) (result i32)
        (if (result i32)
          (block (result i32)
            (call $areCoordsInRange (get_local $x) (get_local $y)))
          (then
            (i32.load
              (call $offsetOf (get_local $x) (get_local $y))))
          (else
            (unreachable))))

  ;; Should the piece at `row` be crowned?
  ;; Black pieces are crowned at row 0, white in row 7.
  (func $shouldCrown (param $row i32) (param $piece i32) (result i32)
        (i32.and
          (i32.eq (call $isCrowned (get_local $piece)) (i32.const 0))
          (i32.or
            (i32.and
              (i32.eq (get_local $row) (i32.const 0))
              (call $isBlack (get_local $piece)))
            (i32.and
              (i32.eq (get_local $row) (i32.const 7))
              (call $isWhite (get_local $piece))))))

  ;; Crowns the piece at (x, y), and invokes a host notifier.
  ;; Illustrates use of a local (stack) variable.
  (func $crownPiece (param $x i32) (param $y i32)
        (local $piece i32)
        (set_local $piece (call $getPiece (get_local $x) (get_local $y)))

        (call $setPiece (get_local $x) (get_local $y)
              (call $addCrown (get_local $piece)))

        (call $notify_pieceCrowned (get_local $x) (get_local $y)))


  ;; Player movement.
    
  ;; Return true if the move ($sourceX, $sourceY) -> ($targetX, $targetY) is valid.
  ;; NB. this code doesn't understand jumps.
  (func $isValidMove
        (param $sourceX i32) (param $sourceY i32)
        (param $targetX i32) (param $targetY i32)
        (result i32)
    (local $source i32)
    (local $target i32)

    ;; Retrieve the board state at the source and target coords
    (set_local $source (call $getPiece (get_local $sourceX) (get_local $sourceY)))
    (set_local $target (call $getPiece (get_local $targetX) (get_local $targetY)))

    ;; The in-book example only checks the number of rows moved is valid.
    ;; We'll do a little better and enforce diagonal movement.
    (i32.and
      (call $isPlayersPiece (get_local $source))
      (i32.and
        (i32.eq (get_local $target) (i32.const 0)) ;; target is empty
        (call $isValidJumpDistance
              (get_local $sourceX) (get_local $sourceY)
              (get_local $targetX) (get_local $targetY)))))

  (func $isValidJumpDistance 
        (param $sourceX i32) (param $sourceY i32)
        (param $targetX i32) (param $targetY i32)
        (result i32)

    (local $dx i32)
    (local $dy i32)
    (local $dxx i32)
    (local $dyy i32)
    (set_local $dx (i32.sub (get_local $targetX) (get_local $sourceX)))
    (set_local $dy (i32.sub (get_local $targetY) (get_local $sourceY)))

    ;; No abs, so test the squared deltas.
    (set_local $dxx (i32.mul (get_local $dx) (get_local $dx)))
    (set_local $dyy (i32.mul (get_local $dy) (get_local $dy)))

    ;; If dxx and dyy are both 1, it's a single diagonal jump.
    ;; If dxx and dyy are both 4, it's a double diagonal jump.
    (i32.and
      (i32.eq (get_local $dxx) (get_local $dyy))
      (i32.or
        (i32.eq (get_local $dxx) (i32.const 1))
        (i32.eq (get_local $dxx) (i32.const 4)))))


  ;; API function for making a move.
  ;; Returns 1 if the move was valid, 0 otherwise.
  (func $move
        (param $sourceX i32) (param $sourceY i32)
        (param $targetX i32) (param $targetY i32)
        (result i32)
    (if (result i32)
      (block (result i32)
        (call $isValidMove (get_local $sourceX) (get_local $sourceY)
                           (get_local $targetX) (get_local $targetY)))
      (then
        (call $do_move (get_local $sourceX) (get_local $sourceY)
                       (get_local $targetX) (get_local $targetY)))
      (else
        (i32.const 0))))

  (func $initGame
    (call $setPiece (i32.const 1) (i32.const 0) (get_global $WHITE))
    (call $setPiece (i32.const 3) (i32.const 0) (get_global $WHITE))
    (call $setPiece (i32.const 5) (i32.const 0) (get_global $WHITE))
    (call $setPiece (i32.const 7) (i32.const 0) (get_global $WHITE))
    (call $setPiece (i32.const 0) (i32.const 1) (get_global $WHITE))
    (call $setPiece (i32.const 2) (i32.const 1) (get_global $WHITE))
    (call $setPiece (i32.const 4) (i32.const 1) (get_global $WHITE))
    (call $setPiece (i32.const 6) (i32.const 1) (get_global $WHITE))

    (call $setPiece (i32.const 1) (i32.const 6) (get_global $BLACK))
    (call $setPiece (i32.const 3) (i32.const 6) (get_global $BLACK))
    (call $setPiece (i32.const 5) (i32.const 6) (get_global $BLACK))
    (call $setPiece (i32.const 7) (i32.const 6) (get_global $BLACK))
    (call $setPiece (i32.const 0) (i32.const 7) (get_global $BLACK))
    (call $setPiece (i32.const 2) (i32.const 7) (get_global $BLACK))
    (call $setPiece (i32.const 4) (i32.const 7) (get_global $BLACK))
    (call $setPiece (i32.const 6) (i32.const 7) (get_global $BLACK))

    (call $setTurnOwner (get_global $BLACK)))

  ;; Internal move implementation, assumes the move has already
  ;; been validated by $move.
  (func $do_move
        (param $sourceX i32) (param $sourceY i32)
        (param $targetX i32) (param $targetY i32)
        (result i32)
    (local $curpiece i32)
    (set_local $curpiece
               (call $getPiece (get_local $sourceX) (get_local $sourceY)))

    (call $switchTurnOwner) ;; XXX: move to higher level logic.

    (call $setPiece (get_local $targetX) (get_local $targetY) (get_local $curpiece))
    (call $setPiece (get_local $sourceX) (get_local $sourceY) (i32.const 0))

    (call $notify_pieceMoved
        (call $getPiece (get_local $targetX) (get_local $targetY))
        (get_local $sourceX) (get_local $sourceY)
        (get_local $targetX) (get_local $targetY))

    (if (call $shouldCrown (get_local $targetY) (get_local $curpiece))
      (then (call $crownPiece (get_local $targetX) (get_local $targetY))))

    (i32.const 1)) ;; implicit return


  ;; API
  (export "getPiece" (func $getPiece))
  (export "getTurnOwner" (func $getTurnOwner))
  (export "isCrowned" (func $isCrowned))
  (export "initGame" (func $initGame))
  (export "move" (func $move))
  (export "memory" (memory $mem))

  ;; Temporary exports for testing
;;  (export "addCrown" (func $addCrown))
;;  (export "areCoordsInRange" (func $areCoordsInRange))
;;  (export "clrBits" (func $clrBits))
;;  (export "crownPiece" (func $crownPiece))
;;  (export "indexOf" (func $indexOf))
;;  (export "isBlack" (func $isBlack))
;;  (export "isPlayersPiece" (func $isPlayersPiece))
;;  (export "isSet" (func $isSet))
;;  (export "isValidJumpDistance" (func $isValidJumpDistance))
;;  (export "isValidMove" (func $isValidMove))
;;  (export "isWhite" (func $isWhite))
;;  (export "offsetOf" (func $offsetOf))
;;  (export "rmCrown" (func $rmCrown))
;;  (export "setBits" (func $setBits))
;;  (export "setPiece" (func $setPiece))
;;  (export "setTurnOwner" (func $setTurnOwner))
;;  (export "shouldCrown" (func $shouldCrown))
;;  (export "switchTurnOwner" (func $switchTurnOwner))
)
