fetch('./checkers.wasm').then(response =>
    response.arrayBuffer()
).then(bytes => WebAssembly.instantiate(bytes, {
    events: {
        piecemoved: (piece, x1, y1, x2, y2) => {
            const pieces = ["?", "Black", "White", "?", "?", "Black(c)", "White(c)"]
            console.log(pieces[piece] + " moved from (" + x1 + ", " + y1 + ") to ("
                        + x2 + ", " + y2 + ")");
        },
        piececrowned: (x, y) => {
            console.log("Piece at (" + x + ", " + y + ") crowned!");
        }
    },
})).then(results => {
    console.log("Loaded WASM module");
    instance = results.instance;

    const players = ["unknown", "black", "white"]
    const black = 1;
    const white = 2;
    const crowned_black = 5;
    const crowned_white = 6;

    instance.exports.initGame();
    console.log("At start, turn owner is " + players[instance.exports.getTurnOwner()]);

    instance.exports.move(4, 7, 6, 5); // B
    instance.exports.move(2, 1, 4, 3); // W
    instance.exports.move(6, 5, 7, 4); // B
    instance.exports.move(4, 3, 6, 5); // W
    instance.exports.move(3, 6, 2, 5); // B
    instance.exports.move(6, 5, 4, 7); // W - crowned
    instance.exports.move(2, 5, 3, 4); // B

    // W - move the crowned piece out
    let res = instance.exports.move(4, 7, 6, 5);

    document.getElementById("container").innerText = res;
    console.log("At end, turn owner is " + players[instance.exports.getTurnOwner()]);
}).catch(console.error);
